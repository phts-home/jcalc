import java.lang.*;
import java.io.*;
import java.util.*;


/**
 * Expression parser.<br><br>
 * 
 * Supported operations: (priority - operation)<br>
 *     0 (lowest)    +, -<br>
 *     1             *, /, % (mod)<br>
 *     2             ^<br>
 *     3 (highest)   !, unary '-'<br><br>
 * 
 * Supported constants:<br>
 *     pi          - The ratio of the circumference of a circle to its diameter (~ 3.1415...)");<br>
 *     e           - The base of the natural logarithms (~ 2.7182...)");<br>
 *     rnd         - A random value with a positive sign, greater than or equal to 0.0 and less than 1.0");<br><br>
 * 
 * Supported functions:<br>
 *     abs         - Returns the absolute value of the argument<br>
 *     round       - Returns the closest value to the argument<br>
 *     sqrt        - Returns the positive square root of the argument<br>
 *     cbrt        - Returns the cube root of the argument<br>
 *     exp         - Returns Euler's number e raised to the power of the argument<br>
 *     ln          - Returns the natural logarithm<br>
 *     log         - Returns the base 10 logarithm<br>
 *     sin         - Returns the trigonometric sine of an angle in radians<br>
 *     cos         - Returns the trigonometric cosine of an angle in radians<br>
 *     tan         - Returns the trigonometric tangent of an angle in radians<br>
 *     asin        - Returns the arc sine of the argument<br>
 *     acos        - Returns the arc cosine of the argument<br>
 *     atan        - Returns the arc tangent of the argument<br>
 *     sinh        - Returns the hyperbolic sine of the argument<br>
 *     cosh        - Returns the hyperbolic cosine - of the argument<br>
 *     tanh        - Returns the hyperbolic tangent of the argument<br>
 *     deg         - Converts an angle measured in radians to an approximately equivalent angle measured in degrees<br>
 *     rad         - Converts an angle measured in degrees to an approximately equivalent angle measured in radians<br>
 * 
 * @author Phil Tsarik
 * @version 1.0.1
 *
 */
public class Expression
{
	
	/**
	 * Constructs a new instance of the <code>Expression</code> class.
	 */
	public Expression()
	{
		val = "";
		res = 0.0;
	}
	
	/**
	 * Gets a result of the expression.
	 * 
	 * @return A result of the expression
	 */
	public double getResult()
	{
		return res;
	}
	
	/**
	 * Gets the expression.
	 * 
	 * @return The expression
	 */
	public String getExpr()
	{
		return val;
	}
	
	/**
	 * Sets a new value of the expression.
	 * 
	 * @param src A new value of the expression
	 */
	public void setExpr(String src)
	{
		val = src;
	}
	
	/**
	 * Caclulates an expression (priority parsing method).
	 * 
	 * @return Error code: 0 - ok; 1 - division by zero; 2 - Unidentified symbol or unknown command;
	 *                     3 - Wrong argument; 4 - Argument is not found; 5 - Operation is not found;
	 *                     6 - Wrong amount of opening/closing brackets; 7 - Wrong amount of operations
	 */
	public int calculateExpr()
	{
		lVal = new ArrayList();           // list of numbers
		lPrior = new ArrayList();         // list of priorities
		lOper = new ArrayList();          // list of operations
		double dNum1 = 0.0;               // first number from list of numbers. to make calculations
		double dNum2 = 0.0;               // second number from list of numbers. to make calculations
		// parse an expression
		int err = parseExpr();
		if (err != 0){
			return err;
		}
		// find max priority
		int maxPr = 0;
		for (int n=0; n<lPrior.size(); n++) {
			if (lPrior.get(n) > maxPr) {
				maxPr = lPrior.get(n);
			}
		}
		// make calculations
		int n;
		for (int cp=maxPr; cp>=0; cp--) {
			n = 0;
			while (n < lVal.size()-1) {
				if (lPrior.get(n) == cp){
					// postfix operations
					// '!' operation
					if (lOper.get(n).equalsIgnoreCase("!")) {
						// get a number
						dNum1 = lVal.get(n);
						// if value is negative or doesn't integer or greater than 12 - raise exception
						if ( (dNum1 < 0.0) || ((double)Math.round(dNum1) != dNum1) || (dNum1 > 12)) {
							//printError(3);
							return 3;
						}
						int iNum = (int)Math.round(dNum1);
						int r = 1;
						for (int num = 2; num <= iNum; num++) {
							r *= num;
						}
						applyOperation((double)r, n);
						continue;
					}
					// unary operations and functions
					// get a number
					dNum1 = lVal.get(n+1);
					// unary '-' operation
					if (lOper.get(n).equalsIgnoreCase("(-)")) {
						applyOperation(-dNum1, n);
						continue;
					}
					// func abs()
					if (lOper.get(n).equalsIgnoreCase("abs")) {
						applyOperation(Math.abs(dNum1), n);
						continue;
					}
					// func round()
					if (lOper.get(n).equalsIgnoreCase("round")) {
						applyOperation((double)Math.round(dNum1), n);
						continue;
					}
					// func sqrt()
					if (lOper.get(n).equalsIgnoreCase("sqrt")) {
						// if value is negative - raise exception
						if (dNum1 < 0.0) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.sqrt(dNum1), n);
						continue;
					}
					// func cbrt()
					if (lOper.get(n).equalsIgnoreCase("cbrt")) {
						applyOperation(Math.cbrt(dNum1), n);
						continue;
					}
					// func exp()
					if (lOper.get(n).equalsIgnoreCase("exp")) {
						applyOperation(Math.exp(dNum1), n);
						continue;
					}
					// func ln()
					if (lOper.get(n).equalsIgnoreCase("ln")) {
						// if value is not positive - raise exception
						if (dNum1 <= 0.0) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.log(dNum1), n);
						continue;
					}
					// func log()
					if (lOper.get(n).equalsIgnoreCase("log")) {
						// if value is not positive - raise exception
						if (dNum1 <= 0.0) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.log10(dNum1), n);
						continue;
					}
					// func sin()
					if (lOper.get(n).equalsIgnoreCase("sin")) {
						applyOperation(Math.sin(dNum1), n);
						continue;
					}
					// func cos()
					if (lOper.get(n).equalsIgnoreCase("cos")) {
						applyOperation(Math.cos(dNum1), n);
						continue;
					}
					// func tan()
					if (lOper.get(n).equalsIgnoreCase("tan")) {
						applyOperation(Math.tan(dNum1), n);
						continue;
					}
					// func asin()
					if (lOper.get(n).equalsIgnoreCase("asin")) {
						if ( (dNum1 < -1.0) || (dNum1 > 1.0) ) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.asin(dNum1), n);
						continue;
					}
					// func acos()
					if (lOper.get(n).equalsIgnoreCase("acos")) {
						if ( (dNum1 < -1.0) || (dNum1 > 1.0) ) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.acos(dNum1), n);
						continue;
					}
					// func atan()
					if (lOper.get(n).equalsIgnoreCase("atan")) {
						if ( (dNum1 < -1.0) || (dNum1 > 1.0) ) {
							//printError(3);
							return 3;
						}
						applyOperation(Math.atan(dNum1), n);
						continue;
					}
					// func sinh()
					if (lOper.get(n).equalsIgnoreCase("sinh")) {
						applyOperation(Math.sinh(dNum1), n);
						continue;
					}
					// func cosh()
					if (lOper.get(n).equalsIgnoreCase("cosh")) {
						applyOperation(Math.cosh(dNum1), n);
						continue;
					}
					// func tanh()
					if (lOper.get(n).equalsIgnoreCase("tanh")) {
						applyOperation(Math.tanh(dNum1), n);
						continue;
					}
					// func deg()
					if (lOper.get(n).equalsIgnoreCase("deg")) {
						applyOperation(Math.toDegrees(dNum1), n);
						continue;
					}
					// func rad()
					if (lOper.get(n).equalsIgnoreCase("rad")) {
						applyOperation(Math.toRadians(dNum1), n);
						continue;
					}
					// binary operations
					// get numbers
					dNum1 = lVal.get(n);
					dNum2 = lVal.get(n+1);
					// '+' operation
					if (lOper.get(n).equalsIgnoreCase("+")) {
						dNum1 += dNum2;
						applyOperation(dNum1, n);
						continue;
					}
					// '-' operation
					if (lOper.get(n).equalsIgnoreCase("-")) {
						dNum1 -= dNum2;
						applyOperation(dNum1, n);
						continue;
					}
					// '*' operation
					if (lOper.get(n).equalsIgnoreCase("*")) {
						dNum1 *= dNum2;
						applyOperation(dNum1, n);
						continue;
					}
					// '/' operation
					if (lOper.get(n).equalsIgnoreCase("/")) {
						if (dNum2 == 0) {
							//printError(1);
							return 1;
						}
						dNum1 /= dNum2;
						applyOperation(dNum1, n);
						continue;
					}
					// '%' operation
					if (lOper.get(n).equalsIgnoreCase("%")) {
						if (dNum2 == 0) {
							//printError(1);
							return 1;
						}
						dNum1 %= dNum2;
						applyOperation(dNum1, n);
						continue;
					}
					// '^' operation
					if (lOper.get(n).equalsIgnoreCase("^")) {
						dNum1 = Math.pow(dNum1,dNum2);
						applyOperation(dNum1, n);
						continue;
					}
				}
				n++;
			}
		}
		// wrong amount of operations check
		if (!lOper.get(0).equals("NULL")){
			//printError(7);
			return 7;
		}
		// get a result of the expression
		res = lVal.get(0);
		// exit
		return 0;
	}
	
	
	private String val;                                // expression
	private double res;                                // result of expression
	private ArrayList <Double> lVal;                   // list of numbers
	private ArrayList <Integer> lPrior;                // list of priorities
	private ArrayList <String> lOper;                  // list of operations
	
	
	/**
	 * Parse an expression.
	 * 
	 * @return Error code: 0 - ok; 1 - division by zero; 2 - Unidentified symbol or unknown command;
	 *                     3 - Wrong argument; 4 - Argument is not found; 5 - Operation is not found;
	 *                     6 - Wrong amount of opening/closing brackets; 7 - Wrong amount of operations
	 */
	private int parseExpr()
	{
		final int PRIORITY_COUNT = 5;                   // amount of different priorities
		int brCnt = 0;                                  // brackets amount
		int i = 0;                                      // counter
		int ii = 0;                                     // auxiliary varible
		int iBeg = 0;                                   // index of beginning of substring
		int iEnd = 0;                                   // index of end of substring
		String sTok = "";                               // substring with token
		// from the beginning to the end of the expression
		while (i<val.length()) {
			switch (val.charAt(i)) {
				case '+':
				case '-':
					// if "-" is unary operation
					if ( (val.charAt(i) == '-') && ((i == 0) || (val.charAt(i-1) == '(')) ) {
						// add blank value to list (does nothing)
						lVal.add(-1.0);
						// add operation and priority to lists
						lOper.add("(-)");
						lPrior.add(3 + brCnt * PRIORITY_COUNT);
						break;
					}
					// add operation and priority to lists
					lOper.add(val.substring(i,i+1));
					lPrior.add(0 + brCnt * PRIORITY_COUNT);
					break;
				case '*':
				case '/':
				case '%':
					// add operation and priority to lists
					lOper.add(val.substring(i,i+1));
					lPrior.add(1 + brCnt * PRIORITY_COUNT);
					break;
				case '^':
					// add operation and priority to lists
					lOper.add(val.substring(i,i+1));
					lPrior.add(2 + brCnt * PRIORITY_COUNT);
					break;
				case '!':
					// add blank value to list (does nothing)
					lVal.add(-1.0);
					// add operation and priority to lists
					lOper.add(val.substring(i,i+1));
					lPrior.add(3 + brCnt * PRIORITY_COUNT);
					break;
				case '(':
					// bracket wrong position check
					ii = i-1;
					while ( (ii > 0) && (val.charAt(ii) == ' ') ) {
						ii--;
					}
					ii++;
					if ( (ii > 0) && (
							(val.charAt(ii-1) == ')')
							|| (val.charAt(ii-1) == '0')
							|| (val.charAt(ii-1) == '1')
							|| (val.charAt(ii-1) == '2')
							|| (val.charAt(ii-1) == '3')
							|| (val.charAt(ii-1) == '4')
							|| (val.charAt(ii-1) == '5')
							|| (val.charAt(ii-1) == '6')
							|| (val.charAt(ii-1) == '7')
							|| (val.charAt(ii-1) == '8')
							|| (val.charAt(ii-1) == '9')
							|| (val.charAt(ii-1) == '.')
							) ) {
						//printError(5);
						return 5;
					}
					// increment brackets amount
					brCnt++;
					break;
				case ')':
					// no argument check
					ii = i-1;
					while ( (ii > 0) && (val.charAt(ii) == ' ') ) {
						ii--;
					}
					ii++;
					if ( (ii <= 0) || (val.charAt(ii-1) == '(') ) {
						//printError(4);
						return 4;
					}
					// amount of '(' and ')' check. if amount_of_'(' <= amount_of_')'
					if (brCnt <= 0){
						//printError(6);
						return 6;
					}
					// decrement brackets amount
					brCnt--;
					break;
				case '0': case '1': case '2': case '3': case '4': case '5':
				case '6': case '7': case '8': case '9': case '.':
					// separate out a number from expression
					iBeg = i;
					while ( (i != val.length()) && (((val.charAt(i) >= '0') && (val.charAt(i) <= '9')) || (val.charAt(i) == '.') || (val.charAt(i) == 'e') || (val.charAt(i) == 'E') || (val.charAt(i) == '+') || (val.charAt(i) == '-')) ) {
						if ( ((val.charAt(i) == '+') || (val.charAt(i) == '-')) && (!((val.charAt(i-1) == 'e') || (val.charAt(i-1) == 'E'))) ){
							break;
						}
						i++;
					}
					iEnd = i;
					i--;
					// add to list of numbers
					lVal.add(Double.parseDouble(val.substring(iBeg,iEnd)));
					break;
				case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
				case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
				case 'm': case 'n': case 'o': case 'p': case 'q': case 'r':
				case 's': case 't': case 'u': case 'v': case 'w': case 'x':
				case 'y': case 'z':
				case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
				case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
				case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
				case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
				case 'Y': case 'Z':
					// separate out a function/constant from expression
					iBeg = i;
					while ( (i != val.length()) && (((val.charAt(i) >= 'a') && (val.charAt(i) <= 'z'))||((val.charAt(i) >= 'A') && (val.charAt(i) <= 'Z'))) ) {
						i++;
					}
					iEnd = i;
					i--;
					sTok = val.substring(iBeg,iEnd);
					// analysis constants
					if (sTok.equalsIgnoreCase("pi")) {
						// add to list of numbers
						lVal.add(Math.PI);
						break;
					}
					if (sTok.equalsIgnoreCase("e")) {
						// add to list of numbers
						lVal.add(Math.E);
						break;
					}
					if (sTok.equalsIgnoreCase("rnd")) {
						// add to list of numbers
						lVal.add(Math.random());
						break;
					}
					// analysis functions
					if ( (sTok.equalsIgnoreCase("abs"))
							|| (sTok.equalsIgnoreCase("round"))
							|| (sTok.equalsIgnoreCase("sqrt"))
							|| (sTok.equalsIgnoreCase("cbrt"))
							|| (sTok.equalsIgnoreCase("exp"))
							|| (sTok.equalsIgnoreCase("ln"))
							|| (sTok.equalsIgnoreCase("log"))
							|| (sTok.equalsIgnoreCase("sin"))
							|| (sTok.equalsIgnoreCase("cos"))
							|| (sTok.equalsIgnoreCase("tan"))
							|| (sTok.equalsIgnoreCase("asin"))
							|| (sTok.equalsIgnoreCase("acos"))
							|| (sTok.equalsIgnoreCase("atan"))
							|| (sTok.equalsIgnoreCase("sinh"))
							|| (sTok.equalsIgnoreCase("cosh"))
							|| (sTok.equalsIgnoreCase("tanh"))
							|| (sTok.equalsIgnoreCase("deg"))
							|| (sTok.equalsIgnoreCase("rad"))
							) {
						// add blank value to list (does nothing)
						lVal.add(-1.0);
						// add operation and priority to lists
						lOper.add(val.substring(iBeg,iEnd));
						lPrior.add(4 + brCnt * PRIORITY_COUNT);
						break;
					}
					// if nothing was found - raise exception
					//printError(2);
					return 2;
				case ' ':
					// ignore spases
					break;
				default:
					// if nothing was found - raise exception
					//printError(2);
					return 2;
			}
			i++;
		}
		// add to lists values of the last number
		lPrior.add(0);
		lOper.add("NULL");
		// amount of '(' and ')' check. if amount_of_'(' != amount_of_')'
		if (brCnt != 0){
			//printError(6);
			return 6;
		}
		// exit
		return 0;
	}
	
	/**
	 * Applies an operation and adds values to lists.
	 */
	private void applyOperation(double val, int n)
	{
		lVal.remove(n);
		lVal.add(n, val);
		lVal.remove(n+1);
		lPrior.remove(n);
		lPrior.add(n, lPrior.get(n));
		lPrior.remove(n+1);
		lOper.remove(n);
		lOper.add(n, lOper.get(n));
		lOper.remove(n+1);
	}
}