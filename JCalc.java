import java.lang.*;
import java.io.*;


/**
 * Main entry program class.
 * 
 * @author Phil Tsarik
 *
 */
public class JCalc {
	
	public static final String name = "JCalc";
	public static final String version = "1.03";
	public static final String year = "2008-2009";
	public static final String author = "Phil Tsarik";
	
	/**
	 * Program entry point.
	 * 
	 * @param args Command line arguments
	 */
	public static void main(String[] argc) {
		printCaption();
		Expression expr = new Expression();
		boolean canExit = false;
		int err;
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		while (!canExit) {
			try {
				System.out.print("> ");
				expr.setExpr(buf.readLine());
				if ( (expr.getExpr().equalsIgnoreCase("help")) || (expr.getExpr().equalsIgnoreCase("?")) ) {
					System.out.println();
					printHelp();
					continue;
				}
				if (expr.getExpr().equalsIgnoreCase("about")) {
					System.out.println();
					printAbout();
					continue;
				}
				if ( (expr.getExpr().equalsIgnoreCase("exit")) || (expr.getExpr().equalsIgnoreCase("quit")) ) {
					canExit = true;
					continue;
				}
				if (expr.getExpr().equals("")) {
					System.out.println();
					continue;
				}
				if ((err = expr.calculateExpr()) == 0){
					System.out.println("\n| "+expr.getResult());
					System.out.println();
				} else {
					printError(err);
				}
			}
			catch (IOException e) {
			}
		}
	}
	
	
	/**
	 * Prints caption on program start.
	 */
	private static void printCaption() {
		System.out.println(JCalc.name + " v" + JCalc.version + "\n(c) " + JCalc.year + " " + JCalc.author);
		System.out.println();
	}
	
	/**
	 * Prints error text depending on the error code.
	 * 
	 * @param code Error code
	 */
	private static void printError(int code) {
		switch (code) {
			case 1:
				System.out.println("\n| MATH ERROR\n| Division by zero");
				break;
			case 2:
				System.out.println("\n| SYNTAX ERROR\n| Unidentified symbol or unknown command");
				break;
			case 3:
				System.out.println("\n| MATH ERROR\n| Wrong argument");
				break;
			case 4:
				System.out.println("\n| SYNTAX ERROR\n| Argument is not found");
				break;
			case 5:
				System.out.println("\n| SYNTAX ERROR\n| Operation is not found");
				break;
			case 6:
				System.out.println("\n| SYNTAX ERROR\n| Wrong amount of opening/closing brackets");
				break;
			case 7:
				System.out.println("\n| SYNTAX ERROR\n| Wrong amount of operations");
				break;
		}
		System.out.println();
	}
	
	/**
	 * Prints Help.
	 */
	private static void printHelp() {
		System.out.println("| HELP");
		System.out.println("|");
		System.out.println("| supported operations: (priority - operation)");
		System.out.println("|       0 (lowest)    +, -");
		System.out.println("|       1             *, /, % (mod)");
		System.out.println("|       2             ^");
		System.out.println("|       3 (highest)   !, unary '-'");
		System.out.println("|");
		System.out.println("| supported commands:");
		System.out.println("|       help, ?     - Show help");
		System.out.println("|       about       - Show about");
		System.out.println("|       exit, quit  - Exit");
		System.out.println("|");
		System.out.println("| supported constants:");
		System.out.println("|       pi          - The ratio of the");
		System.out.println("|                     circumference of a circle to");
		System.out.println("|                     its diameter (~ 3.1415...)");
		System.out.println("|       e           - The base of the natural");
		System.out.println("|                     logarithms (~ 2.7182...)");
		System.out.println("|       rnd         - A random value with a");
		System.out.println("|                     positive sign, greater than");
		System.out.println("|                     or equal to 0.0 and less");
		System.out.println("|                     than 1.0");
		System.out.println("|");
		System.out.println("| supported functions:");
		System.out.println("|       abs         - Returns the absolute value of");
		System.out.println("|                     the argument");
		System.out.println("|       round       - Returns the closest value to");
		System.out.println("|                     the argument");
		System.out.println("|       sqrt        - Returns the positive square");
		System.out.println("|                     root of the argument");
		System.out.println("|       cbrt        - Returns the cube root of the");
		System.out.println("|                     argument");
		System.out.println("|       exp         - Returns Euler's number e");
		System.out.println("|                     raised to the power of the");
		System.out.println("|                     argument");
		System.out.println("|       ln          - Returns the natural logarithm");
		System.out.println("|       log         - Returns the base 10 logarithm");
		System.out.println("|       sin         - Returns the trigonometric");
		System.out.println("|                     sine of an angle in radians");
		System.out.println("|       cos         - Returns the trigonometric");
		System.out.println("|                     cosine of an angle in radians");
		System.out.println("|       tan         - Returns the trigonometric");
		System.out.println("|                     tangent of an angle in");
		System.out.println("|                     radians");
		System.out.println("|       asin        - Returns the arc sine of the");
		System.out.println("|                     argument");
		System.out.println("|       acos        - Returns the arc cosine of the");
		System.out.println("|                     argument");
		System.out.println("|       atan        - Returns the arc tangent of");
		System.out.println("|                     the argument");
		System.out.println("|       sinh        - Returns the hyperbolic sine");
		System.out.println("|                     of the argument");
		System.out.println("|       cosh        - Returns the hyperbolic cosine");
		System.out.println("|                     of the argument");
		System.out.println("|       tanh        - Returns the hyperbolic");
		System.out.println("|                     tangent of the argument");
		System.out.println("|       deg         - Converts an angle measured in");
		System.out.println("|                     radians to an approximately");
		System.out.println("|                     equivalent angle measured in");
		System.out.println("|                     degrees");
		System.out.println("|       rad         - Converts an angle measured in");
		System.out.println("|                     degrees to an approximately");
		System.out.println("|                     equivalent angle measured in");
		System.out.println("|                     radians");
		System.out.println("|");
		System.out.println("| functions syntax  : func_name(value)");
		System.out.println();
	}
	
	/**
	 * Prints About.
	 */
	private static void printAbout() {
		System.out.println("| ABOUT");
		System.out.println("|");
		System.out.println("| " + JCalc.name + " " + JCalc.version);
		System.out.println("|");
		System.out.println("| (c) " + year + " " + JCalc.author);
		System.out.println();
	}
}